import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from "./app.component";
import { HomePageComponent } from "./home/home-page/home-page.component";
import { MaterialModule } from "./material-ui.module";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NavbarComponent } from './home/navbar/navbar.component';
import { HeaderComponent } from './home/header/header.component';
import {DynamicDialogModule} from 'primeng/dynamicdialog';
import {DialogModule} from 'primeng/dialog';
import { SectionAdoptComponent } from './home/section-adopt/section-adopt.component';
import { SectionGarderComponent } from './home/section-garder/section-garder.component';
import { SectionDogWolkingComponent } from './home/section-dog-wolking/section-dog-wolking.component';
import { SectionFoundHomeComponent } from './home/section-found-home/section-found-home.component';
import { ConnectionComponent } from './home/Modals/connection/connection.component';
import {TableModule} from 'primeng/table';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { InscriptionComponent } from './home/Modals/inscription/inscription.component';

import { ReactiveFormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { ToastrModule } from 'ngx-toastr';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import { AdoptionPageComponent } from './adoption/adoption-page/adoption-page.component';
import { CardComponent } from './adoption/card/card.component';
import { AdoptRequestComponent } from './adoption/Modals/adopt-request/adopt-request.component';

@NgModule({
  imports: [
    BrowserModule,
    AppRoutingModule,
    DynamicDialogModule,
    DialogModule,
    MaterialModule,
    BrowserAnimationsModule,
    TableModule,
    NgbModule.forRoot(),
    ReactiveFormsModule ,
    CommonModule,
    BrowserAnimationsModule, // required animations module
    ToastrModule.forRoot(),
    HttpClientModule
  ],
  declarations: [AppComponent, HomePageComponent, NavbarComponent,
    HeaderComponent, SectionAdoptComponent, SectionGarderComponent,
    SectionDogWolkingComponent, SectionFoundHomeComponent,
    ConnectionComponent,
    InscriptionComponent,
    AdoptionPageComponent,
    CardComponent,
    AdoptRequestComponent],
  bootstrap: [AppComponent],
  entryComponents: [
    ConnectionComponent, InscriptionComponent, AdoptRequestComponent
  ],
  providers: [
    NgbActiveModal,
    HttpClient
  ]
})
export class AppModule {}
