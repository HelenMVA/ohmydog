import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdoptRequestComponent } from './adopt-request.component';

describe('AdoptRequestComponent', () => {
  let component: AdoptRequestComponent;
  let fixture: ComponentFixture<AdoptRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdoptRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdoptRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
