import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from "@ng-bootstrap/ng-bootstrap";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../../services/user.service";
import {AdoptionService} from "../../../services/adoption.service";
import {ToastrService} from "ngx-toastr";

@Component({
  selector: 'app-adopt-request',
  templateUrl: './adopt-request.component.html',
  styleUrls: ['./adopt-request.component.scss']
})
export class AdoptRequestComponent implements OnInit {
  @Input() public currentUser;
  @Input() public dog;


  FormAdoption: FormGroup = this.fb.group({
    phone: ['', Validators.required],
    reason: ['', Validators.required],
  });
  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private userService: AdoptionService,private toastService: ToastrService) { }

  ngOnInit() {
    console.dir(this.currentUser);
    console.dir(this.dog);
  }

  closeModal() {
    this.activeModal.close();
  }

  adopt() {
    const id_user = this.currentUser.data.user.id;
    const id_dog = this.dog.id;
    const phone = this.FormAdoption.controls.phone.value;
    const description = this.FormAdoption.controls.reason.value
    const dog = {
      id_dog,
      id_user,
      phone,
      description
    }
    this.userService.adopter(dog).subscribe(data => {
          this.toastService.success('Hello', this.currentUser.data.user.firstname);
        }, (err) => {
          this.toastService.error(err);
        }
    );
  }
}
