import { Component, OnInit } from '@angular/core';
import {AdoptionService} from '../../services/adoption.service';
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {AdoptRequestComponent} from "../Modals/adopt-request/adopt-request.component";

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss']
})
export class CardComponent implements OnInit {
  data: any;
  allDogs: any;
  currentUser;
  dog

  constructor( private adoptionService: AdoptionService, private modalService: NgbModal) { }

  ngOnInit() {
    this.getAdoptionDogs();
    this.getCurrentUser();
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  getAdoptionDogs() {
    this.adoptionService.getAdoptionDogs().subscribe((res) => {
      this.data = res;
      this.allDogs = this.data.message;
      console.dir(this.data.message);
    });
  }

  showAdoptModal(dog) {
    console.dir(dog);
    const modalRef = this.modalService.open(AdoptRequestComponent);
    modalRef.componentInstance.currentUser = this.currentUser;
    modalRef.componentInstance.dog = dog;
  }

}
