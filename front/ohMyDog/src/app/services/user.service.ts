import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {User} from '../models/user';
import {catchError, mapTo, tap} from 'rxjs/operators';
import { CanActivate, Router } from '@angular/router';
import { Observable } from 'rxjs';
import * as jwt_decode from 'jwt-decode';

@Injectable({
  providedIn: 'root'
})
export class UserService implements CanActivate {
  apiURL = 'http://localhost:8000/back/' ;

  constructor(private http: HttpClient, private  router: Router) { }

  register(user: User) {
    console.log(user);
    return this.http.post(`${this.apiURL}users/register`, user);
  }

  connect(connectUser: User){
    return this.http.post(`${this.apiURL}users/authenticate`, connectUser)
        .pipe(
            tap(user => this.doLogin(user)),
            mapTo(true),
            catchError(err => err)

        );
  }

  doLogin(user: any) {
    localStorage.setItem('currentUser', JSON.stringify(user));
    // this.isLoggedIn();
  }
  isLoggedIn() {
    const currentUser = this.getCurrentUser();
    if (currentUser) {
      const token = this.getDecodeToken(currentUser.token);
      console.dir (token);
      const currentTime = Math.round((new Date()).getTime() / 1000);
      console.dir(currentTime);
      // if (token.exp > currentTime) {
      //   return true;
      // } else {
      //   this.logOut();
      // }
    }
    return  false;
  }
  getCurrentUser() {
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  getDecodeToken(token: string) {
    return jwt_decode(token);
  }
  logOut() {
    localStorage.removeItem('currentUser');
  }
  canActivate() {
    const currentUser = this.getCurrentUser();
    console.dir(currentUser.data.user.user_role);
    if (currentUser) {
      return true;
    } else {
      console.dir(false);
      return false; }
  }


}
