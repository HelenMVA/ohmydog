import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomePageComponent } from './home/home-page/home-page.component';
import {AdoptionPageComponent} from './adoption/adoption-page/adoption-page.component';
import {UserService} from './services/user.service';

const routes: Routes = [{ path: '', component: HomePageComponent },
  { path: 'adoption', component: AdoptionPageComponent, canActivate: [UserService] }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
