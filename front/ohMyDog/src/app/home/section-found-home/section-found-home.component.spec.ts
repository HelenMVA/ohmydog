import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionFoundHomeComponent } from './section-found-home.component';

describe('SectionFoundHomeComponent', () => {
  let component: SectionFoundHomeComponent;
  let fixture: ComponentFixture<SectionFoundHomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionFoundHomeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionFoundHomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
