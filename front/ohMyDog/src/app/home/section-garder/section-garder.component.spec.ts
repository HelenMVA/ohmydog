import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionGarderComponent } from './section-garder.component';

describe('SectionGarderComponent', () => {
  let component: SectionGarderComponent;
  let fixture: ComponentFixture<SectionGarderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionGarderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionGarderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
