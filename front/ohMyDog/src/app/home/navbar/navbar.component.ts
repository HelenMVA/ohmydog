import { Component, OnInit } from '@angular/core';

import {NgbModal, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import {ConnectionComponent} from '../Modals/connection/connection.component';
import {InscriptionComponent} from '../Modals/inscription/inscription.component';
import {UserService} from '../../services/user.service';



@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit {
  currentUser;

  constructor(private modalService: NgbModal, private userService: UserService) { }

  ngOnInit() {
    this.getCurrentUser();
  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return JSON.parse(localStorage.getItem('currentUser'));
  }

  showConnectionModal() {
    const modalRef = this.modalService.open(ConnectionComponent);
    // modalRef.componentInstance.user = this.currentUser;
    // modalRef.componentInstance.fromParent = data;
    modalRef.result.then((result) => {
      this.currentUser = result;
    });
  }
  showInscriptionModal() {
    const modalRef = this.modalService.open(InscriptionComponent);
  }
  logout() {
    this.userService.logOut();
    this.getCurrentUser();
  }
}
