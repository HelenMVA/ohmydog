import {Component, Input, OnInit} from '@angular/core';
import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';
import {UserService} from '../../../services/user.service';
@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.scss']
})
export class InscriptionComponent implements OnInit {
  @Input() fromParent;


  FormRegister: FormGroup = this.fb.group({
    firstname: ['', Validators.compose([Validators.required])],
    lastname: ['', Validators.compose([Validators.required])],
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });

  constructor(public activeModal: NgbActiveModal, private fb: FormBuilder, private router: Router, private toastService: ToastrService, private userService: UserService) { }

  ngOnInit() {
  }
  register() {
    console.dir(this.FormRegister.value);
    this.userService.register(this.FormRegister.value).subscribe(data => {
          this.toastService.success('User creted', '');
          this.closeModal();
        }, (err) => {
          this.toastService.error(err);
        }
    );
  }


  closeModal() {
    this.activeModal.close();
  }
}
