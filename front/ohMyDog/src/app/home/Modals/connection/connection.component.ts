import { Component, Input, OnInit } from '@angular/core';
import {NgbModal, NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Validators, FormBuilder, FormGroup, FormControl} from "@angular/forms";
import {ToastrService} from "ngx-toastr";
import {UserService} from '../../../services/user.service';



@Component({
  selector: 'app-connection',
  templateUrl: './connection.component.html',
  styleUrls: ['./connection.component.scss']
})
export class ConnectionComponent implements OnInit {
  currentUser;
  // @Input() public user;

  FormConnection: FormGroup = this.fb.group({
    email: ['', Validators.compose([Validators.required, Validators.email])],
    password: ['', Validators.compose([Validators.required, Validators.minLength(6)])],
  });
  constructor(
      public activeModal: NgbActiveModal, private fb: FormBuilder, private toastService: ToastrService, private userService: UserService
  ) {}


  ngOnInit() {
    // console.dir(this.user);
  }
  connect(){
    this.userService.connect(this.FormConnection.value).subscribe(data => {
      this.getCurrentUser();
      this.toastService.success('Hello', this.currentUser.data.user.firstname);
      this.closeModal(this.currentUser);
        }, (err) => {
          this.toastService.error(err);
        }
    );

  }
  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return JSON.parse(localStorage.getItem('currentUser'));
  }
  closeModal(currentUser) {
    this.activeModal.close(currentUser);
  }
}
