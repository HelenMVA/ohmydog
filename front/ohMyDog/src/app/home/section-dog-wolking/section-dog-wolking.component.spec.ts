import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionDogWolkingComponent } from './section-dog-wolking.component';

describe('SectionDogWolkingComponent', () => {
  let component: SectionDogWolkingComponent;
  let fixture: ComponentFixture<SectionDogWolkingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionDogWolkingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionDogWolkingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
