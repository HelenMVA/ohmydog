import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";
import {ConnectionComponent} from "../Modals/connection/connection.component";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ToastrService} from "ngx-toastr";


@Component({
  selector: 'app-section-adopt',
  templateUrl: './section-adopt.component.html',
  styleUrls: ['./section-adopt.component.scss']
})
export class SectionAdoptComponent implements OnInit {
  currentUser;
  constructor(private router: Router, private modalService: NgbModal, private toastService: ToastrService) { }

  ngOnInit() {
  }

  getCurrentUser() {
    this.currentUser = JSON.parse(localStorage.getItem('currentUser'));
    return JSON.parse(localStorage.getItem('currentUser'));
  }
  goAdoptPage(){
    this.getCurrentUser()
    if(this.currentUser){
      this.router.navigate(['/adoption']);
    } else {
      this.toastService.error('veuillez vous connecter');
      // this.showConnectionModal();
    }
  }

  // showConnectionModal() {
  //   const modalRef = this.modalService.open(ConnectionComponent);
  //   modalRef.result.then((result) => {
  //     this.currentUser = result;
  //   });
  // }
}
