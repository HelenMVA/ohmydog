import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SectionAdoptComponent } from './section-adopt.component';

describe('SectionAdoptComponent', () => {
  let component: SectionAdoptComponent;
  let fixture: ComponentFixture<SectionAdoptComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SectionAdoptComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SectionAdoptComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
