import express from "express";
import usersRoutes from "../modules/user/routes";
import adoptionRoutes from "../modules/adoption/routes";


const Router = (server) => {
    //Project router
     server.use("/back/users", usersRoutes);
    server.use("/back/adoption", adoptionRoutes);
};

export default Router;
