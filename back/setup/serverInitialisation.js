import express from "express";
import bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import morgan from "morgan";
import router from "./router";
// import databaseInitialisation from "./databaseInitialisation";

const serverInitialisation = (server) => {
    console.info('SETUP - Loading modules...')
    //console.log(process.env)
    // databaseInitialisation()
    server.use(bodyParser.json())
    server.use(bodyParser.urlencoded({ extended: false }))

    // Request body cookie parser
    server.use(cookieParser());

    server.use(morgan("tiny"));
    // Enable CORS
    server.use(function (req, res, next) {
        res.setHeader("Access-Control-Allow-origin", "http://localhost:4200");
        res.setHeader(
            "Access-Control-Allow-Headers",
            "X-Requested-With, content-type, application/json, Authorization"
        );
        res.setHeader("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE");
        res.setHeader("Access-Control-Allow-Credentials", true);
        next();
    });
    
    // Initializing our routes
    router(server);
};

export default serverInitialisation;
