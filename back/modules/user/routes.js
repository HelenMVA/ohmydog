import express from "express";
const router = express.Router();

import UserController from "./controller";


//Public routes
router.post("/authenticate", UserController.authenticate);
router.post("/register", UserController.register);

export default router;
