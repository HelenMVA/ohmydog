import UserServices from "./service";

const UserController = {
    getUserWithToken: (req, res) => {
        UserServices.getUserWithToken(req).then((result) =>
            res.status(result.status).send(result.payload)
        );
    },
    authenticate: (req, res) => {
        UserServices.authenticate(req.body).then((result) =>
            res.status(result.status).send(result.payload)
        );
    },
    register: async (req, res) => {
        UserServices.register(req.body).then((result) =>
            res.status(result.status).send(result.payload)
        );
    },
};

export default UserController;
