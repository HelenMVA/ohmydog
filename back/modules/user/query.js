import db from "../../setup/database";

// Notre query s'occupe d'effectuer la requête sur la base de donneés et de renvoyer au service les datas
const Queries = {
    // authenticate: (user, successCallback, failureCallback) => {
    //     let sqlQuery = `SELECT * FROM users WHERE name="${user.email}" AND password="${user.password}"`;
    //
    //     db.query(sqlQuery, (err, rows) => {
    //         if (err) {
    //             return failureCallback(err);
    //         }
    //         if (rows.length > 0) {
    //             return successCallback(rows[0]);
    //         } else {
    //             return successCallback("Incorrect username or password combinaison");
    //         }
    //     });
    // },
    getByUserEmail: (email) => {
        let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;

        return new Promise((resolve, reject) => {
            db.query(sqlQuery, (err, rows) => {
                if (err) reject(err);
                resolve(rows[0]);
            });
        });
    },
    getByUsername: (email) => {
        let sqlQuery = `SELECT * FROM users WHERE email="${email}"`;

        return new Promise((resolve, reject) => {
            db.query(sqlQuery, (err, rows) => {
                if (err) reject(err);
                resolve(rows[0]);
            });
        });
    },
    getUserInformationsByUserId: (id) => {
        let sqlQuery = `SELECT *
    from users WHERE id = "${id}"`;

        return new Promise((resolve, reject) => {
            db.query(sqlQuery, (err, rows) => {
                if (err) reject(err);
                resolve(rows[0]);
            });
        });
    },
    register: async (users) => {
        return new Promise((resolve, reject) => {
            let sqlQuery = `INSERT INTO users (id, firstname, lastname, email, password, user_role) VALUES (NULL,"${users.firstname}","${users.lastname}", "${users.email}", "${users.hashedPassword}", "user")`;

            db.query(sqlQuery, (err, res) => {
                if (err) reject(err);
                resolve(res);
            });
        });
    },
};

export default Queries;
