import express from "express";
const router = express.Router();

import AdoptionController from "./controller";
import authorize from "../../helpers/autorize"


//Public routes
// router.get("/", authorize(['user']), AdoptionController.getAll);
router.get("/", AdoptionController.getAll);
router.post("/request", AdoptionController.adoptDog);

export default router;
