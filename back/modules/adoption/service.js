import AdoptionQueries from './query';
import UserQueries from "../user/query";
import bcrypt from "bcrypt";

const AdoptionServices = {
    getAll: (req, callback) => {
        AdoptionQueries.getAll(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        );
    },

    adoptDog: (req, callback)  => {
        AdoptionQueries.adoptDog(
            req,
            response => {
                return callback({ success: true, message: response });
            },
            error => {
                return callback({ success: false, message: error });
            }
        );
    },

}

export default AdoptionServices
