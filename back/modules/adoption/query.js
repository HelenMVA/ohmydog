import db from '../../setup/database';


const Queries = {
    getAll: (param, successCallback, failureCallback) => {
        let sqlQuery = 'SELECT * FROM `adoption`';

        db.query(sqlQuery, (err, rows) => {
            if (err) {
                return failureCallback(err);
            }
            if (rows.length > 0) {
                return successCallback(rows);
            } else {
                return successCallback('No Dogs.');
            }
        });
    },

    adoptDog:(param, successCallback, failureCallback) => {
        console.log(param.body)
            let sqlQuery = `INSERT INTO user_has_adoption (id, id_dog, id_user, phone, description) VALUES (NULL,"${param.body.id_dog}","${param.body.id_user}", "${param.body.phone}", "${param.body.description}")`;

            db.query(sqlQuery, (err) => {
                if (err) {
                    return failureCallback(err);
                } else {
                    return successCallback('Adoption enregistrer')
                }
            });
    },
}

export default Queries;
